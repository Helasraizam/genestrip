#!/usr/bin/env python3

import sys,re
from math import *
import cairo
#from matplotlib import pyplot as plt
#import numpy as np

class GeneStrip():
    def __init__(self,gseq,rsites,eseq,filetype='svg',size=(5578,155)):
        self.gseq=gseq
        self.rsites=self.loadrsites(rsites)
        self.eseq=self.loadexons(eseq)
        self.W,self.H=size
        self.length=len(gseq)
        self.exmatches=self.findexons()
        self.rsmatches=self.findrsites()
        self.highlightcolor=[0,0,1]
        self.exhighlights=[0]*len(self.exmatches)
        self.filetype=filetype

    def loadexons(self,txt):
        return [q.strip().split('\n') for q in txt.strip().split('>')][1:]

    def findexons(self):
        exmatches=[]
        for exon in self.eseq:
            for m in re.finditer(exon[1],self.gseq):
                exmatches.append([exon[0].split(' ')[1].split(':')[1],m.start(),m.end()])
        return sorted(exmatches,key=lambda m: m[1])

    def loadrsites(self,txt):
        return [q.split(',') for q in txt.strip().split('\n')]

    def findrsites(self):
        """For now, only checks + strand."""
        rsmatches=[]
        for rs in self.rsites:
            regex=rs[1].replace('^','').replace('_','').replace('N','.')
            for m in re.finditer(regex,self.gseq):
                rsmatches.append([rs[0],m.start()+rs[1].replace('_','').index('^')])
        return sorted(rsmatches,key=lambda m: m[1])

    def savefile(self,filename,title=None):
        """Save the image of the strip(s) with padding and title."""
        if self.filetype=='svg':
            self.drawstrip('%s.%s'%(filename,self.filetype))
        elif self.filetype=='png':
            self.drawstrip().write_to_png('%s.%s'%(filename,self.filetype))

    def sethighlightcolor(self,color=[0,0,1]):
        self.highlightcolor=color

    def highlightexon(self,index,color=None):
        """[color]=[r,g,b], 0<=r<=1"""
        self.exhighlights[index-1]=1 if color==None else color

    def plotexon(self,ctx,index,scale=(21/20,5)):
        """ctx is cairo context, index is exon index as presented in plot (i+1)."""
        #surface=cairo.SVGSurface(savefile,#cairo.FORMAT_ARGB32,
        #                         int(self.W*scale[0]),self.H*scale[1])
        #ctx=cairo.Context(surface)
        i=index
        m=self.exmatches[i]

        if self.exhighlights[i]!=0:
            hc=self.highlightcolor if type(self.exhighlights[i])!=list else self.exhighlights[i]
            ctx.set_source_rgb(*hc)
        
        ctx.rectangle(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length,
                      int((scale[1]-1)/2*self.H),
                      (m[2]-m[1])*self.W/self.length,self.H)
        ctx.fill()
        ctx.set_source_rgb(0,0,0)

        (_,_,width,height,_,_)=ctx.text_extents(str(i+1))
        (_,_,fheight,_,_)=ctx.font_extents()
        ctx.move_to(int((scale[0]-1)/2*self.W)+(m[1]+m[2])/2*self.W/self.length-width/2,
                    int((scale[1]+1)/2*self.H+self.H/4+10))
        ctx.show_text(str(i+1))

        mm=m[0].split('..')
        (_,_,width,height,_,_)=ctx.text_extents(mm[0])
        (_,_,fheight,_,_)=ctx.font_extents()
        ctx.save()
        ctx.set_line_width(3)
        ctx.move_to(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length,
                    int((scale[1]-1)/2*self.H+self.H))
        ctx.line_to(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length,
                    int((scale[1]-1)/2*self.H+1.8*self.H))
        ctx.line_to(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length+(m[2]-m[1])*self.W/self.length+20,
                    int((scale[1]-1)/2*self.H+1.8*self.H))
        ctx.stroke()
        ctx.restore()
        ctx.line_to(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length+(m[2]-m[1])*self.W/self.length+20,
                    int((scale[1]-1)/2*self.H+1.8*self.H+fheight/4))
        ctx.show_text(mm[0])

        ctx.save()
        ctx.set_line_width(3)
        ctx.move_to(int((scale[0]-1)/2*self.W)+m[2]*self.W/self.length,
                    int((scale[1]-1)/2*self.H+self.H))
        ctx.line_to(int((scale[0]-1)/2*self.W)+m[2]*self.W/self.length,
                    int((scale[1]-1)/2*self.H+1.5*self.H))
        ctx.line_to(int((scale[0]-1)/2*self.W)+m[2]*self.W/self.length+20,
                    int((scale[1]-1)/2*self.H+1.5*self.H))
        ctx.stroke()
        ctx.restore()
        ctx.line_to(int((scale[0]-1)/2*self.W)+m[2]*self.W/self.length+20,
                    int((scale[1]-1)/2*self.H+1.5*self.H+fheight/4))
        ctx.show_text(mm[1])
        
        if False:
            mm=m[0].split('..')
            (_,_,width,height,_,_)=ctx.text_extents(mm[0])
            (_,_,fheight,_,_)=ctx.font_extents()
            ctx.move_to(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length,
                        int((scale[1]-1)/2*self.H)-self.H/2)
            ctx.save()
            ctx.rotate(-pi/4)
            ctx.translate(width,fheight)
            ctx.show_text(mm[0])
            ctx.restore()

            ctx.move_to(int((scale[0]-1)/2*self.W)+m[2]*self.W/self.length,
                        int((scale[1]-1)/2*self.H)-self.H/2)
            ctx.save()
            ctx.rotate(-pi/4)
            ctx.show_text(mm[0])
            ctx.restore()
        
    def drawstrip(self,filename=None,scale=(21/20,5)):
        """Return surface of single strip."""
        if self.filetype=='svg':
            surface=cairo.SVGSurface(filename,#cairo.FORMAT_ARGB32,
                                     int(self.W*scale[0]),self.H*scale[1])
        elif self.filetype=='png':
            surface=cairo.ImageSurface(cairo.FORMAT_ARGB32,
                                  int(self.W*scale[0]),self.H*scale[1])

        ctx=cairo.Context(surface)
        
        ctx.set_source_rgb(1,1,1)
        ctx.rectangle(0,0,self.W*scale[0],self.H*scale[1])
        ctx.fill()

        ctx.set_line_width(5)
        ctx.set_source_rgb(0,0,0)
        ctx.rectangle(int((scale[0]-1)/2*self.W),
                      int((scale[1]-1)/2*self.H),self.W,self.H)
        ctx.stroke()

        ctx.select_font_face("DejaVu Sans")
        ctx.set_font_size(40)
        for i,m in enumerate(self.exmatches):
            self.plotexon(ctx,i,scale)

        for m in self.rsmatches:
            ctx.rectangle(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length,
                          int((scale[1]-1)/2*self.H),
                          max(int(self.W/self.length),2),self.H)
            ctx.fill()

            ctx.save()
            (_,_,width,height,_,_)=ctx.text_extents(m[0])
            (_,_,fheight,_,_)=ctx.font_extents()
            ctx.translate(int((scale[0]-1)/2*self.W)+m[1]*self.W/self.length,
                          int((scale[1]-1)/2*self.H-10))
            ctx.rotate(-pi/4)
            #ctx.translate(width/2,fheight/2)
            ctx.show_text(m[0])
            ctx.restore()

        ctx.show_page()

        return surface

    def padimage(self,surface,scale=(21/20,6)):
        """Pad Image (scale background up to [scale]), add title."""
        surface2=cairo.ImageSurface(cairo.FORMAT_ARGB32,int(self.W*scale[0]),self.H*scale[1])
        
        ctx2=cairo.Context(surface2)
        ctx2.set_source_rgb(1,1,1)
        ctx2.rectangle(0,0,self.W*scale[0],self.H*scale[1])
        ctx2.fill()
        ctx2.save()
        ctx2.set_source_surface(surface,int(self.W/40),scale[1]/2*self.H)
        ctx2.paint()
        ctx2.restore()

        return surface2


def main():
    with open('geneseq.txt','r') as f:
        gseq=f.read()
    with open('restrictionsties.txt','r') as f:
        rsites=f.read()
    with open('exonseq.txt','r') as f:
        eseq=f.read()

    gs=GeneStrip(gseq,rsites,eseq,filetype='svg')
    gs.highlightexon(5)
    gs.highlightexon(6)
    gs.highlightexon(8,color=[1,0,0])
    gs.savefile('test1')
    
    gs=GeneStrip(gseq,rsites,eseq,filetype='png')
    gs.sethighlightcolor([0,1,1])
    gs.highlightexon(5)
    gs.highlightexon(6)
    gs.highlightexon(8,color=[1,0,0])
    gs.savefile('test2')


if __name__=='__main__':
    main()
